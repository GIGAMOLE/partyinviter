package app

import app.Params.defaultInputPath
import app.Params.defaultInviteDistance
import app.Params.defaultOfficeLatitude
import app.Params.defaultOfficeLongitude
import app.Params.defaultOutputFileName
import app.Params.defaultOutputPath
import app.Params.earthRadius
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import data.Customer
import java.awt.Desktop
import java.io.File
import java.io.IOException
import java.lang.IllegalStateException
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


object Params {
    const val earthRadius = 6371.0

    const val defaultInviteDistance = 100.0
    const val defaultOfficeLatitude = 53.339428
    const val defaultOfficeLongitude = -6.257664

    const val defaultInputPath = "src/assets/input.txt"
    const val defaultOutputPath = "out/"
    const val defaultOutputFileName = "output.txt"
}

fun main() {
    val inputFile = readInput(defaultInputPath)

    val customers = parseInput(inputFile)
    val invitedCustomers = inviteCustomersWithinOfficeArea(
        customers,
        defaultInviteDistance,
        defaultOfficeLatitude,
        defaultOfficeLongitude
    )

    val outputFile = writeOutput(
        formatInvitedCustomers(invitedCustomers),
        defaultOutputPath,
        defaultOutputFileName
    )
    openOutput(outputFile)
}

fun readInput(path: String): File {
    val inputFile = File(path)
    if (!inputFile.exists()) throw NullPointerException("Input file doest exist, please create it")

    return inputFile
}

fun parseInput(inputFile: File): List<Customer> {
    val gson = Gson()

    val lines = inputFile.readLines()
    if (lines.isEmpty()) throw IllegalStateException("Input file is empty")

    try {
        return lines.map { line ->
            gson.fromJson(line, Customer::class.java)
        }
    } catch (e: JsonSyntaxException) {
        throw IllegalStateException("Input file contains wrong structure")
    }
}

fun inviteCustomersWithinOfficeArea(
    customers: List<Customer>,
    inviteDistance: Double,
    officeLatitude: Double,
    officeLongitude: Double
): List<Customer> = customers.filter { customer ->
    if (customer.latitude == null || customer.longitude == null) return@filter false

    val customerDistance = calculateGreatCircleDistance(
        officeLatitude,
        officeLongitude,
        customer.latitude.toDouble(),
        customer.longitude.toDouble()
    )
    validateInviteDistance(inviteDistance, customerDistance)
}

fun calculateGreatCircleDistance(
    latitude1: Double,
    longitude1: Double,
    latitude2: Double,
    longitude2: Double
): Double {
    val latitudeRadians1 = Math.toRadians(latitude1)
    val latitudeRadians2 = Math.toRadians(latitude2)
    val deltaLatitude = Math.toRadians(latitude2 - latitude1)
    val deltaLongitude = Math.toRadians(longitude2 - longitude1)

    val haversine = sin(deltaLatitude / 2.0) * sin(deltaLatitude / 2.0) +
            cos(latitudeRadians1) * cos(latitudeRadians2) *
            sin(deltaLongitude / 2.0) * sin(deltaLongitude / 2.0)
    val angle = 2.0 * atan2(sqrt(haversine), sqrt(1.0 - haversine))

    return earthRadius * angle
}

fun validateInviteDistance(inviteDistance: Double, customerDistance: Double) =
    customerDistance <= inviteDistance

fun formatInvitedCustomers(
    invitedCustomers: List<Customer>
): List<String> = invitedCustomers
    .filter { customer -> customer.name != null && customer.userId != null }
    .sortedBy { customer -> customer.userId }
    .map { customer -> "${customer.name}'s party invitation code is: ${customer.userId}" }

fun writeOutput(lines: List<String>, path: String, fileName: String): File {
    val outputPath = File(path)
    outputPath.mkdirs()

    val outputFile = File(path, fileName)
    if (!outputFile.exists()) outputFile.createNewFile()
    if (!outputFile.exists()) throw NullPointerException("Output file doest exist, something went wrong")

    outputFile.printWriter().use { printWriter ->
        lines.forEach { line -> printWriter.println(line) }
    }

    return outputFile
}

fun openOutput(outputFile: File) {
    if (Desktop.isDesktopSupported()) {
        try {
            Desktop.getDesktop().open(outputFile)
        } catch (e: IOException) {
            println("The file doesn't exist or no software to open it")
        }
    }
}