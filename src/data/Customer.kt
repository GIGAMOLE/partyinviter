package data

import com.google.gson.annotations.SerializedName

/*{
    "latitude": "52.986375",
    "user_id": 12,
    "name": "Christina McArdle",
    "longitude": "-6.043701"
}*/

data class Customer(
    @SerializedName("user_id")
    val userId: Long?,
    val name: String?,
    val latitude: String?,
    val longitude: String?
)