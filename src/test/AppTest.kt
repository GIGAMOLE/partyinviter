package test

import app.*
import data.Customer
import org.junit.Test
import java.io.File
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class AppTest {

    @Test
    fun `when read input file doesnt exist`() {
        assertFails {
            readInput("src/test/input.txt")
        }
    }

    @Test
    fun `when parse input file is empty`() {
        assertFails {
            parseInput(File("src/test/empty-input.txt"))
        }
    }

    @Test
    fun `when parse input file is wrong`() {
        assertFails {
            parseInput(File("src/test/wrong-input.txt"))
        }
    }

    @Test
    fun `when parse input file is success`() {
        val customers = parseInput(File("src/test/success-input.txt"))
        assert(customers.isNotEmpty())
    }

    @Test
    fun `when invite with zero distance then no customers`() {
        assert(
            inviteCustomersWithinOfficeArea(
                listOf(
                    Customer(0, "", "52.986375", "-6.043701"),
                    Customer(0, "", "52.986375", "-6.043701")
                ),
                0.0,
                53.339428,
                -6.257664
            ).isEmpty()
        )
    }

    @Test
    fun `when invite with zero office location then no customers`() {
        assert(
            inviteCustomersWithinOfficeArea(
                listOf(
                    Customer(0, "", "52.986375", "-6.043701"),
                    Customer(0, "", "52.986375", "-6.043701")
                ),
                100.0,
                0.0,
                0.0
            ).isEmpty()
        )
    }

    @Test
    fun `when calculate great circle is correct`() {
        assertTrue(
            calculateGreatCircleDistance(
                53.339428,
                -6.257664,
                52.986375,
                -6.043701
            ) == 41.7687255008362
        )
    }

    @Test
    fun `when calculate great circle is wrong`() {
        assertFalse(
            calculateGreatCircleDistance(
                53.339428,
                -6.257664,
                52.986375,
                -6.043701
            ) == 42.7687255008362
        )
    }

    @Test
    fun `when validate invite distance is correct`() {
        assertTrue(
            validateInviteDistance(
                100.0,
                50.0
            )
        )
    }

    @Test
    fun `when validate invite distance is wrong`() {
        assertFalse(
            validateInviteDistance(
                100.0,
                250.0
            )
        )
    }

    @Test
    fun `when format invited customers is correct`() {
        val invitedCustomers = listOf(
            Customer(2, "two", "", ""),
            Customer(1, "one", "", "")
        )
        val formatInvitedCustomers = formatInvitedCustomers(invitedCustomers)
        assertTrue(
            formatInvitedCustomers[0].contains("1") &&
                    formatInvitedCustomers[0].contains("one") &&
                    formatInvitedCustomers[1].contains("2") &&
                    formatInvitedCustomers[1].contains("two")
        )
    }
}