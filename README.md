# Revolut Rates

This is the challenge project for the position of Android Developer. Created in a simple way using Intellij Idea and Java/Kotlin based structure. The task is to parse the input file of customers, filter them using the great-circle distance formula and invite them to the party if they within the valid office area.

Have a nice day :)

## Getting Started

- Download and install [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- Follow the instructions and install all components if necessary
- Download the repository or clone it with `git`
- Open the project with IntelliJ IDEA and wait confirm all libs download

## App

- To change the input file, follow to `/src/assets` folder
- The output file will be generated in `/out` folder
- The App will automatically open the output file after execution

## Launch

App:  
1. Open the `App.kt` file  
2. Run the app by clicking on green arrow near `main` function  

![](/images/1.png)
  
  
  
Tests:  
1. Open the `AppTest.kt` file  
2. Run the tests by clicking on green arrow near `AppTest` class definition  
  
![](/images/2.png)

## Author

[Basil Miller](https://www.linkedin.com/in/gigamole/)  
[+380 50 569 8419](tel:380505698419)  
[gigamole53@gmail.com](mailto:gigamole53@gmail.com)
